# README #

A small library of utilities for use with SDL.

* Smart pointer wrappers for most SDL Primitives and some common libraries.
* A bunch of SDL_Color definitions for common colors.
* A function for calculating an AABB for a rotated SDL_Rect

## Setup

* Not sure yet, you do need SDL2 at least.