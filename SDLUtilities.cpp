#include "SDLUtilities.h"

namespace sdl2
{

WindowPtr createWindow(const std::string& title, int width, int height, int x, int y, Uint32 flags)
{
	SDL_Window* window = SDL_CreateWindow(title.c_str(), x, y, width, height, flags);

	if (!window)
	{
		throw std::runtime_error(SDL_GetError());
	}

	return WindowPtr(window);
}

RendererPtr createRenderer(const WindowPtr& pxWindow, Uint32 flags, int index)
{
	SDL_Renderer* renderer = SDL_CreateRenderer(pxWindow.get(), index, flags);

	if (!renderer)
	{
		throw std::runtime_error(SDL_GetError());
	}

	return RendererPtr(renderer);
}



SurfacePtr loadSurfaceFromBMP(const std::string& pFilename)
{
	SDL_Surface* surface = SDL_LoadBMP(pFilename.c_str());

	if (!surface)
	{
		throw std::runtime_error(SDL_GetError());
	}

	return SurfacePtr(surface);
}

SurfacePtr loadSurfaceIMG(const std::string& pFilename)
{
	SDL_Surface* surface = IMG_Load(pFilename.c_str());

	if (!surface)
	{
		throw std::runtime_error(IMG_GetError());
	}

	return SurfacePtr(surface);
}


TexturePtr createTextureFromSurface(const RendererPtr& pxRenderer, const SurfacePtr& pxSurface)
{
	SDL_Texture* texture = SDL_CreateTextureFromSurface(pxRenderer.get(), pxSurface.get());

	if (!texture)
	{
		throw std::runtime_error(SDL_GetError());
	}

	return TexturePtr(texture);
}


TexturePtr loadTextureFromBMP(const RendererPtr& pxRenderer, const std::string& pFilename)
{
	return createTextureFromSurface(pxRenderer, loadSurfaceFromBMP(pFilename));
}

TexturePtr loadTextureIMG(const RendererPtr& pxRenderer, const std::string & pFilename)
{
	return createTextureFromSurface(pxRenderer, loadSurfaceIMG(pFilename));
}



MixChunkPtr loadMixChunk(const std::string& pFilname)
{
	Mix_Chunk* chunk = Mix_LoadWAV(pFilname.c_str());

	if (!chunk)
	{
		throw std::runtime_error(Mix_GetError());
	}

	return MixChunkPtr(chunk);
}

MixMusicPtr loadMixMusic(const std::string& pFilename)
{
	Mix_Music* music = Mix_LoadMUS(pFilename.c_str());

	if (!music)
	{
		throw std::runtime_error(Mix_GetError());
	}

	return MixMusicPtr(music);
}

TTFFontPtr loadTTFFont(const std::string& pFilename, const int pSize)
{
	TTF_Font* ptr = TTF_OpenFont(pFilename.c_str(), pSize);

	if (!ptr)
	{
		throw std::runtime_error(TTF_GetError());
	}

	return TTFFontPtr(ptr);
}

SurfacePtr renderTTFTextSolid(const TTFFontPtr& pFont, const std::string& pText, const SDL_Color& pColor)
{
	auto ptr = TTF_RenderText_Solid(pFont.get(), pText.c_str(), pColor);

	if (!ptr)
	{
		throw std::runtime_error(TTF_GetError());
	}

	return SurfacePtr(ptr);
}

TexturePtr renderTTFTextSolid(const TTFFontPtr& pFont, const std::string& pText, const SDL_Color& pColor, const RendererPtr& pRenderer)
{
	return createTextureFromSurface(pRenderer, renderTTFTextSolid(pFont, pText, pColor));
}

// Takes angle in radians
const SDL_Rect& calculateAABB(const SDL_Rect& alignedRect, const double angle)
{
	SDL_Rect ret(alignedRect);

	// Bounding box calculation from:
	// http://www.willperone.net/Code/coderr.php
	//w' = h*abs(sin(a)) + w*abs(cos(a))
	//h' = h*abs(cos(a)) + w*abs(sin(a))
	ret.w = alignedRect.h*abs(sin(angle)) +
		      alignedRect.w*abs(cos(angle));

	ret.h = alignedRect.h*abs(cos(angle)) +
		      alignedRect.w*abs(sin(angle));

	return ret;
}

} // namespace sdl2
