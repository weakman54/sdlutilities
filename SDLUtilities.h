/*
 * SDL Utilities
 * Written/Gathered by Erik Wallin
 *
 * Has smart pointer wrappers for most SDL primitives, plus some other libraries
 *
 * Also some colors defined
 *
 * Smart pointer wrapper code from http://swarminglogic.com/jotting/2015_05_smartwrappers
 * (Slightly extended and modified by ErikW)
 *
 * Thanks to Ulf Benjaminson for reminding me of this!
*/

#pragma once

#include <memory>

namespace sdl2
{

// Deleter functor for SDL primitives and some libraries.
// Used as the Deleter for unique_ptr
struct SDL_Deleter
{
	void operator()(SDL_Surface*  ptr) { if (ptr) SDL_FreeSurface(ptr); }
	void operator()(SDL_Texture*  ptr) { if (ptr) SDL_DestroyTexture(ptr); }
	void operator()(SDL_Renderer* ptr) { if (ptr) SDL_DestroyRenderer(ptr); }
	void operator()(SDL_Window*   ptr) { if (ptr) SDL_DestroyWindow(ptr); }
	void operator()(SDL_RWops*    ptr) { if (ptr) SDL_RWclose(ptr); }

	void operator()(Mix_Chunk*    ptr) { if (ptr) Mix_FreeChunk(ptr); } // requires SDL_mixer library
	void operator()(Mix_Music*    ptr) { if (ptr) Mix_FreeMusic(ptr); } // requires SDL_mixer library

	void operator()(TTF_Font*     ptr) { if (ptr) TTF_CloseFont(ptr); } // requires SDL_TTF library
};


// Unique Pointers
using WindowPtr   = std::unique_ptr<SDL_Window, SDL_Deleter>;
using RendererPtr = std::unique_ptr<SDL_Renderer, SDL_Deleter>;
using SurfacePtr  = std::unique_ptr<SDL_Surface, SDL_Deleter>;
using TexturePtr  = std::unique_ptr<SDL_Texture, SDL_Deleter>;
using RWopsPtr    = std::unique_ptr<SDL_RWops, SDL_Deleter>;

using MixChunkPtr = std::unique_ptr<Mix_Chunk, SDL_Deleter>; // requires SDL_mixer library
using MixMusicPtr = std::unique_ptr<Mix_Music, SDL_Deleter>; // requires SDL_mixer library

using TTFFontPtr  = std::unique_ptr<TTF_Font, SDL_Deleter>; // requires SDL_TTF library


// Shared ptr with deleter
template<class T, class D = std::default_delete<T>>
struct shared_ptr_with_deleter : public std::shared_ptr<T>
{
	explicit shared_ptr_with_deleter(T* t = nullptr)
		: std::shared_ptr<T>{ t, D() }
	{}

	void reset(T* t = nullptr)
	{
		std::shared_ptr<T>::reset(t, D());
	}
};

// Shared pointers
using SurfaceShPtr  = shared_ptr_with_deleter<SDL_Surface, SDL_Deleter>;
using TextureShPtr  = shared_ptr_with_deleter<SDL_Texture, SDL_Deleter>;
using RendererShPtr = shared_ptr_with_deleter<SDL_Renderer, SDL_Deleter>;
using WindowShPtr   = shared_ptr_with_deleter<SDL_Window, SDL_Deleter>;
using RWopsShPtr    = shared_ptr_with_deleter<SDL_RWops, SDL_Deleter>;

using MixChunkShPtr = shared_ptr_with_deleter<Mix_Chunk, SDL_Deleter>; // requires SDL_mixer library
using MixMusicShPtr = shared_ptr_with_deleter<Mix_Music, SDL_Deleter>; // requires SDL_mixer library

using TTFFontShPtr  = shared_ptr_with_deleter<TTF_Font, SDL_Deleter>; // requires SDL_mixer library


// SDL creator-function wrappers
// TODO: inline?
WindowPtr createWindow(const std::string& title, int width, int height,
	int x = SDL_WINDOWPOS_UNDEFINED,
	int y = SDL_WINDOWPOS_UNDEFINED,
	Uint32 flags = SDL_WINDOW_OPENGL);

RendererPtr createRenderer(const WindowPtr&, Uint32 flags = SDL_RENDERER_ACCELERATED, int index = -1);

TexturePtr createTextureFromSurface(const RendererPtr& pxRenderer, const SurfacePtr& pxSurface);


SurfacePtr loadSurfaceFromBMP(const std::string& pFilename);
SurfacePtr loadSurfaceIMG(const std::string& pFilename); // requires SDL_image library

TexturePtr loadTextureFromBMP(const RendererPtr& pxRenderer, const std::string& pFilename);
TexturePtr loadTextureIMG(const RendererPtr& pxRenderer, const std::string& pFilename); // requires SDL_image library


MixChunkPtr loadMixChunk(const std::string& pFilname); // requires SDL_mixer library
MixMusicPtr loadMixMusic(const std::string& pFilname); // requires SDL_mixer library


TTFFontPtr loadTTFFont(const std::string& pFilname, const int pSize); // requires SDL_TTF library
SurfacePtr renderTTFTextSolid(const TTFFontPtr& pFont, const std::string& pText, const SDL_Color& pColor); // requires SDL_TTF library TODO: fix background color?
TexturePtr renderTTFTextSolid(const TTFFontPtr& pFont, const std::string& pText, const SDL_Color& pColor, const RendererPtr& pRenderer); // requires SDL_TTF library TODO: fix background color?



// AABB collision function
const SDL_Rect& calculateAABB(const SDL_Rect& alignedRect, const double angle);



namespace color
{

const SDL_Color Black{ 0x00, 0x00, 0x00 };
const SDL_Color Red  { 0xff, 0x00, 0x00 };
const SDL_Color Green{ 0x00, 0xff, 0x00 };
const SDL_Color Blue { 0x00, 0x00, 0xff };
const SDL_Color White{ 0xff, 0xff, 0xff };

} // namespace color

} // namespace sdl2
